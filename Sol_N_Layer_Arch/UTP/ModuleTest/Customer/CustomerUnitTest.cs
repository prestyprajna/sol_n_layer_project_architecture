﻿using BAL.Context1.Customer;
using Entity.Model;
using Entity.Model.Customer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UTP.ModuleTest.Customer
{
    [TestClass]
    public class CustomerUnitTest
    {
        [TestMethod]
        public void AddCustomerTestMethod()
        {
            Task.Run(async() =>
            {
                var entityObj = new CustomerEntity()
                {
                    personEntityObj = new PersonEntity()
                    {
                        FirstName = "diwik",
                        LastName = "suvarna",
                        loginEntityObj = new LoginEntity()
                        {
                            Username = "diwik",
                            Password = "345"
                        },
                        communicationEntityObj = new CommunicationEntity()
                        {
                            MobileNo = "8108990725",
                            EmailId = "diwik@gmail.com"
                        }
                    }
                };


                var result=await new CustomerContext()?.CustomerRegistration(entityObj);

                Assert.IsNotNull(result);

            }).Wait();
        }

        [TestMethod]
        public void CustomerLoginTestMethod()
        {
            Task.Run(async () =>
            {
                var entityObj = new CustomerEntity()
                {
                    personEntityObj = new PersonEntity()
                    {                       
                        loginEntityObj = new LoginEntity()
                        {
                            Username = "123",
                            Password = "345"
                        }                       
                    }
                };


                var result = await new CustomerContext()?.CustomerLogin(entityObj);

                Assert.IsTrue(result);

            }).Wait();
        }


    }
}
