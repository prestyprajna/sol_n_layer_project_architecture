﻿using DAL.Repository.IRepository.Customer;
using DAL.Repository.Repository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repository_Factory.Customer
{
    public static class CustomerRepositoryFactory
    {
        #region  enum

        public enum RepositoyType
        {
            AddCustomerRepositoy=0,
            LoginCustomerRepository=1
            //CustomerCollectionRepository=2
        };

        #endregion

        #region  declaration

        private static Dictionary<RepositoyType, dynamic> _dicObj = new Dictionary<RepositoyType, dynamic>();
       
        #endregion

        #region  constructor

        static CustomerRepositoryFactory()
        {
            AddInstance();
        }

        #endregion

        #region  private methods

        private static void AddInstance()
        {
            _dicObj.Add(RepositoyType.AddCustomerRepositoy, new Lazy<IAddCustomerRepository>(() => AddCustomerRepository.CreateInstance.Value ));
            _dicObj.Add(RepositoyType.LoginCustomerRepository, new Lazy<ILoginCustomerRepository>(() => LoginCustomerRepository.CreateLoginInstance.Value));
            //_dicObj.Add(RepositoyType.CustomerCollectionRepository, new Lazy<ICustomerCollectionRepository>(() => CustomerCollectionRepository.CreateCustomerCollectionInstance.Value));
            
        }

        #endregion

        #region  public methods

        public static TRepository ExecuteRepository<TRepository>(RepositoyType repositoryTypeObj) where TRepository:class
        {
            return _dicObj[repositoryTypeObj].Value as TRepository;
        }

        #endregion


    }
}
