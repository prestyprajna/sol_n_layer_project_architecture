﻿using DAL.Repository.IRepository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using DAL.ORM.Concrete.Customer;
using DAL.ORM.Factory;
using DAL.ORM.Interface.IConcrete.Customer;

namespace DAL.Repository.Repository.Customer
{
    public class LoginCustomerRepository : ILoginCustomerRepository
    {
        #region  declaration

        private static Lazy<ILoginCustomerRepository> _loginCustomerRepositoryObj = null;

        private Lazy<CustomerConcrete> _customerConcreteObj = null;

        #endregion

        #region  constructor

        private LoginCustomerRepository()
        {

        }

        #endregion

        #region  property

        public  static Lazy<ILoginCustomerRepository> CreateLoginInstance
        {
            get
            {
                return
                    _loginCustomerRepositoryObj ??
                    (_loginCustomerRepositoryObj = new Lazy<ILoginCustomerRepository>(() => new LoginCustomerRepository()));
            }
        }

        private Lazy<CustomerConcrete> CreateCustomerInstance
        {
            get
            {
                return
                  _customerConcreteObj ??
                  (_customerConcreteObj = new Lazy<CustomerConcrete>(() =>FactoryConcrete.ExecuteFactory<CustomerConcrete>(FactoryConcrete.ConcreteType.Customer)));
            }
        }

        #endregion

        #region  public methods

        public async Task<dynamic> CustomerLogin(ICustomerEntity customerEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                await CreateCustomerInstance?.Value?.SetDataAsync(
                    "LOGIN",
                    customerEntityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    }
                    );

                return (status == 1) ? true : false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Dispose()
        {
            _customerConcreteObj?.Value?.Dispose();
            _customerConcreteObj = null;
            _loginCustomerRepositoryObj = null;
        }

        #endregion



    }
}
