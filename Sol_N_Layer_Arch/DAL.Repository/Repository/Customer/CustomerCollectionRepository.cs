﻿using DAL.Repository.IRepository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using DAL.ORM.Concrete.Customer;
using DAL.ORM.Factory;
using DAL.ORM.Interface.IConcrete.Customer;
using Entity.Model.Customer;
using Entity.Model;

namespace DAL.Repository.Repository.Customer
{
    public class CustomerCollectionRepository : ICustomerCollectionRepository
    {
        #region  declaration

        private static Lazy<CustomerCollectionRepository> _customerCollectionRepositoryObj = null;
        private Lazy<CustomerConcrete> _customerConcreteObj = null;

        #endregion

        #region  constructor

        private CustomerCollectionRepository()
        {

        }

        #endregion

        #region  property

        public static Lazy<CustomerCollectionRepository> CreateCustomerCollectionInstance
        {
            get
            {
                return
                    _customerCollectionRepositoryObj??
                    (_customerCollectionRepositoryObj = new Lazy<CustomerCollectionRepository>(()=>new CustomerCollectionRepository()));
            }
        }

        private Lazy<CustomerConcrete> CustomerConcreteInstance
        {
            get
            {
                return
                    _customerConcreteObj ??
                    (_customerConcreteObj = new Lazy<CustomerConcrete>(()=>FactoryConcrete.ExecuteFactory<CustomerConcrete>(FactoryConcrete.ConcreteType.Customer)));

            }
        }


        #endregion

        #region  public methods

        public async Task<ICustomerEntity> GetCustomerCommunicationData(ICustomerEntity customerEntityObj)
        {
            int? status = null;
            string message = null;

            try
            {
                var getQuery = 
                    (await CustomerConcreteInstance?.Value?.GetDataAsync(
                    "GetMobileNoSMS",
                    customerEntityObj,
                    (leUspGetCustomerObj) => new CustomerEntity()
                    {
                        personEntityObj = new PersonEntity()
                        {
                            communicationEntityObj = new CommunicationEntity()
                            {
                                PersonId = leUspGetCustomerObj?.PersonId,
                                MobileNo = leUspGetCustomerObj?.MobileNo,
                                EmailId = leUspGetCustomerObj?.EmailId
                            }
                        }

                    }))
                    ?.FirstOrDefault();
                    
            
                return getQuery;
                    
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Dispose()
        {
            _customerConcreteObj?.Value?.Dispose();
            _customerConcreteObj = null;
            _customerCollectionRepositoryObj = null;
        }

        #endregion




    }
}
