﻿using DAL.Repository.IRepository.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using DAL.ORM.Concrete.Customer;
using DAL.ORM.Factory;
using DAL.ORM.ORD.Customer;
using System.Data.Linq;
using Entity.Model.Customer;
using Entity.Model;

namespace DAL.Repository.Repository.Customer
{
    public class AddCustomerRepository : IAddCustomerRepository
    {
        #region  declaration

        private static Lazy<AddCustomerRepository> _addCustomerRepositoryObj = null;
        private Lazy<CustomerConcrete> _customerConcreteObj = null;

        private Lazy<CustomerCollectionRepository> _customerCollectionRepositoryObj = null;

        #endregion

        #region  constructor
        private AddCustomerRepository()
        {

        }

        #endregion

        #region  property

        private Lazy<CustomerConcrete> CustomerCreateInstance
        {
            get
            {
                return
                _customerConcreteObj = new Lazy<CustomerConcrete>(() => FactoryConcrete.ExecuteFactory<CustomerConcrete>(FactoryConcrete.ConcreteType.Customer));
            }
        }

        public static Lazy<AddCustomerRepository> CreateInstance
        {
            get
            {
                return
                    _addCustomerRepositoryObj ??
                    (_addCustomerRepositoryObj = new Lazy<AddCustomerRepository>(() => new AddCustomerRepository()));

            }
        }

        public Lazy<CustomerCollectionRepository> CustomerCollectionRepositoryInstance
        {
            get
            {
                return
                  _customerCollectionRepositoryObj ??
                  (_customerCollectionRepositoryObj = new Lazy<CustomerCollectionRepository>(() => CustomerCollectionRepository.CreateCustomerCollectionInstance.Value));
            }
        }

        private async Task<dynamic> GetPersonIdAsync(dynamic query)
        {
            try
            {
                return await Task.Run(() =>
                {
                    ISingleResult<uspGetCustomerResultSet> customerResultSet = query as ISingleResult<uspGetCustomerResultSet>;

                    return customerResultSet
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.SingleOrDefault()
                    ?.PersonId;
                });
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        #endregion


        #region  public methods
        public async Task<dynamic> AddCustomerRegistration(ICustomerEntity customerEntityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                var setQuery=await CustomerCreateInstance?.Value?.
                    SetDataAsync("INSERT",
                    customerEntityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    }
                    );

                //get personid
                decimal? personId = await this.GetPersonIdAsync(setQuery);

                ICustomerEntity tempCustomerEntityObj = await CustomerCollectionRepositoryInstance
                    ?.Value
                    ?.GetCustomerCommunicationData(new CustomerEntity()
                    {
                        personEntityObj = new PersonEntity()
                        {
                            PersonId=personId
                        }
                    });
                    

                return (status == 1) ? tempCustomerEntityObj : (dynamic)false;


            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Dispose()
        {
            _customerConcreteObj?.Value?.Dispose();
            _customerConcreteObj = null;
            _addCustomerRepositoryObj = null;
        }

        #endregion


    }
}
