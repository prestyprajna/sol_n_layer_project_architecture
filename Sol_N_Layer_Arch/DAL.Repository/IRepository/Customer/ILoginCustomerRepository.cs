﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.IRepository.Customer
{
    public interface ILoginCustomerRepository:IDisposable
    {
        Task<dynamic> CustomerLogin(ICustomerEntity customerEntityObj);
    }
}
