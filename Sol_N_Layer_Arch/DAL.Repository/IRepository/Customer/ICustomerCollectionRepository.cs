﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.IRepository.Customer
{
    public interface ICustomerCollectionRepository
    {
        Task<ICustomerEntity> GetCustomerCommunicationData(ICustomerEntity customerEntityObj);
    }
}
