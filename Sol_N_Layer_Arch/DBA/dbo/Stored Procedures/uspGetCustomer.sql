﻿CREATE PROCEDURE [dbo].[uspGetCustomer]
	@Command VARCHAR(50),
	
	@PersonId NUMERIC(18,0),
	@FirstName VARCHAR(10),
	@LastName VARCHAR(100),

	@MobileNo VARCHAR(10),
	@EmailId VARCHAR(100),

	@Username VARCHAR(10),
	@Password VARCHAR(100),

	@Status INT=NULL OUT,
	@Message VARCHAR(MAX)=NULL OUT	

AS
	DECLARE @ErrorMessage VARCHAR(MAX)

	BEGIN

		IF @Command='GetMobileNoSMS'
			BEGIN

				BEGIN TRANSACTION

				BEGIN TRY

					SELECT C.PersonId,
					C.MobileNo,
					C.EmailId
						FROM tblCommunication AS C
							WHERE C.PersonId=@PersonId


							SET @Status=1
							SET @Message='GET DATA SUCCESSFULL'

							COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()				 
					SET @Status=1
					SET @Message='GET DATA SUCCESSFULL'

					ROLLBACK TRANSACTION

					RAISERROR(@ErrorMessage,16,1)
					

				END CATCH


			END

		


	END
	
GO