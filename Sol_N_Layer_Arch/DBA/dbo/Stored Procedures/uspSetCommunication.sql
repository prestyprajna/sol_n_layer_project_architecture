﻿CREATE PROCEDURE [dbo].[uspSetCommunication]
	
	@Command VARCHAR(50),
	
	@PersonId NUMERIC(18,0),
	@MobileNo VARCHAR(10),
	@EmailId VARCHAR(100),

	@Status INT=NULL OUT,
	@Message VARCHAR(MAX)=NULL OUT

AS

	BEGIN
		DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION

					BEGIN TRY
						
						INSERT INTO tblCommunication
						(
							PersonId,
							MobileNo,
							EmailId
						)
						VALUES
						(
							@PersonId,
							@MobileNo,
							@EmailId
						)

						SET @Status=1
						SET @Message='INSERT SUCCESFULL'

						COMMIT TRANSACTION
			

					END TRY

					BEGIN CATCH

						SET @ErrorMessage=ERROR_MESSAGE()
						SET @Status=0
						SET @Message='INSERT UNSUCCESFULL'

						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)



					END CATCH
	
				

			END

	
	
	END