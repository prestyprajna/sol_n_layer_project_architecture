﻿CREATE PROCEDURE [dbo].[uspSetPerson]
	
	@Command VARCHAR(50),
	
	@PersonId NUMERIC(18,0),
	@FirstName VARCHAR(10),
	@LastName VARCHAR(100),

	@Status INT=NULL OUT,
	@Message VARCHAR(MAX)=NULL OUT,
	@Id NUMERIC(18,0)=NULL OUT

AS

	BEGIN
		DECLARE @ErrorMessage varchar(50)

		IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION

					BEGIN TRY
						
						INSERT INTO tblPerson
						(							
							FirstName,
							LastName
						)
						VALUES
						(							
							@FirstName,
							@LastName
						)

						SET @Id=@@IDENTITY

						SET @Status=1
						SET @Message='INSERT SUCCESFULL'

						COMMIT TRANSACTION
			

					END TRY

					BEGIN CATCH

						SET @ErrorMessage=ERROR_MESSAGE()
						SET @Status=0
						SET @Message='INSERT UNSUCCESFULL'

						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)



					END CATCH
	
				

			END

	
	
	END