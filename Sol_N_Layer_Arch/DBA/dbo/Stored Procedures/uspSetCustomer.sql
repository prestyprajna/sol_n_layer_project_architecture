﻿CREATE PROCEDURE [dbo].[uspSetCustomer]
		
	@Command VARCHAR(50),
	
	@PersonId NUMERIC(18,0),
	@FirstName VARCHAR(10),
	@LastName VARCHAR(100),

	@MobileNo VARCHAR(10),
	@EmailId VARCHAR(100),

	@Username VARCHAR(10),
	@Password VARCHAR(100),

	@Status INT=NULL OUT,
	@Message VARCHAR(MAX)=NULL OUT	

AS

	BEGIN

		DECLARE @ErrorMessage varchar(50)
		DECLARE @Id NUMERIC(18,0)


		IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION

					BEGIN TRY

						EXEC uspSetPerson @Command,NULL,@FirstName,@LastName,NULL,NULL,@Id out

						EXEC uspSetCommunication @Command,@Id,@MobileNo,@EmailId,NULL,NULL

						EXEC uspSetLogin @Command,@Id,@Username,@Password,NULL,NULL
						
						INSERT INTO tblCustomer
						(							
							PersonId							
						)
						VALUES
						(							
							@Id
						)

						SET @Id=@@IDENTITY

						SET @Status=1
						SET @Message='INSERT SUCCESFULL'

						COMMIT TRANSACTION
			

					END TRY

					BEGIN CATCH

						SET @ErrorMessage=ERROR_MESSAGE()
						SET @Status=0
						SET @Message='INSERT UNSUCCESFULL'

						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)



					END CATCH
	
				

			END

		ELSE IF @Command='LOGIN'
			BEGIN

				BEGIN TRANSACTION

				BEGIN TRY

					SELECT @PersonId=L.PersonId
						FROM tblLogin AS L
							WHERE L.Username=@Username
							AND 
							L.Password=@Password

					IF @PersonId IS NOT NULL
						BEGIN 
							SET @Status=1
							SET @Message='LOGIN SUCCESS'
						END
					ELSE
						BEGIN
							SET @Status=0
							SET @Message='LOGIN NOT SUCCESS'	
						END

						COMMIT TRANSACTION
				END TRY


				BEGIN CATCH
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'
					
					ROLLBACK TRANSACTION


					RAISERROR(@ErrorMessage,16,1)
					
				END CATCH

				

			END	
	
	END