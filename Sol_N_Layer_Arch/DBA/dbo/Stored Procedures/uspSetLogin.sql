﻿CREATE PROCEDURE [dbo].[uspSetLogin]
	@Command VARCHAR(50),
	
	@PersonId NUMERIC(18,0),
	@Username VARCHAR(10),
	@Password VARCHAR(100),

	@Status INT=NULL OUT,
	@Message VARCHAR(MAX)=NULL OUT

AS

	BEGIN
		DECLARE @ErrorMessage varchar(MAX)

		IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION

					BEGIN TRY
						
						INSERT INTO tblLogin
						(
							PersonId,
							Username,
							Password
						)
						VALUES
						(
							@PersonId,
							@Username,
							@Password
						)

						SET @Status=1
						SET @Message='INSERT SUCCESFULL'

						COMMIT TRANSACTION
			

					END TRY

					BEGIN CATCH

						SET @ErrorMessage=ERROR_MESSAGE()
						SET @Status=0
						SET @Message='INSERT UNSUCCESFULL'

						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)



					END CATCH
	
				

			END

	
	
	END