﻿using DAL.ORM.Interface.ICommon;
using DAL.ORM.ORD.Customer;
using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.IConcrete.Customer
{
    public interface ICustomerConcrete:ISetRepository<ICustomerEntity>,IGetRepository<uspGetCustomerResultSet,ICustomerEntity>, IDisposable
    {
    }
}
