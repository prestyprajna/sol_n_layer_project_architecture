﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.ICommon
{
    public interface IGetRepository<TLinqEntity,TEntity> where TLinqEntity : class where TEntity : class
    {
        Task<dynamic> GetDataAsync(string command, TEntity entityObj,Func<TLinqEntity,TEntity> selector ,Action<int?, string> storedProcOutPara = null);
    }
}
