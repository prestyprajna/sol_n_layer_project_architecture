﻿using DAL.ORM.Concrete.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Factory
{
    public static class FactoryConcrete
    {
        #region enum

        public enum ConcreteType
        {
            Customer=0
        };

        #endregion

        #region  declaration

        private static Dictionary<ConcreteType, dynamic> _dicObj = new Dictionary<ConcreteType, dynamic>();

        //private Lazy<CustomerConcrete> _customerConcreteObj = null;

        #endregion

        #region  constructor
        static FactoryConcrete()
        {
            AddInstance();
        }

        #endregion

        #region  public methods

        private static void AddInstance()
        {
            _dicObj.Add(ConcreteType.Customer,new Lazy<CustomerConcrete>(() => CustomerConcrete.CreateInstance.Value));
        }

        public static TConcrete ExecuteFactory<TConcrete>(ConcreteType concreteTypeObj) where TConcrete:class
        {
            return _dicObj[concreteTypeObj].Value as TConcrete;
        }


        #endregion

    }
}
