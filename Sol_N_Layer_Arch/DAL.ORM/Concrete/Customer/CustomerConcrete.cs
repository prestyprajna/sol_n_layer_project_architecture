﻿using DAL.ORM.Interface.IConcrete.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using DAL.ORM.ORD.Customer;

namespace DAL.ORM.Concrete.Customer
{
    public class CustomerConcrete : ICustomerConcrete
    {
        #region  declaration

        private static Lazy<CustomerConcrete> _customerConcreteObj = null;
        private Lazy<CustomerDCDataContext> _dc = null;

        #endregion

        #region  Constructor
        private CustomerConcrete()
        {

        }

        #endregion

        #region  public methods

        private Lazy<CustomerDCDataContext> CreateDCInstance
        {
            get
            {
                return
                    _dc ??
                    (_dc = new Lazy<CustomerDCDataContext>(() => new CustomerDCDataContext()));
            }
        }

        public static Lazy<CustomerConcrete> CreateInstance
        {
            get
            {
                return
                _customerConcreteObj ??
                (_customerConcreteObj = new Lazy<CustomerConcrete>(() => new CustomerConcrete()));
            }
            
        }


        public void Dispose()
        {
            _dc?.Value?.Dispose();
            _dc = null;
            _customerConcreteObj = null;

        }

        public async Task<dynamic> SetDataAsync(string command, ICustomerEntity entityObj, Action<int?, string> storedProcOutPara = null)
        {
            try
            {
                int? status = null;
                string message = null;

                return await Task.Run(() =>
                {
                    var setQuery =
                    CreateDCInstance?.Value?.
                    uspSetCustomer(
                        command,
                        entityObj?.personEntityObj?.PersonId,
                        entityObj?.personEntityObj?.FirstName,
                        entityObj?.personEntityObj?.LastName,
                        entityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                        entityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                        entityObj?.personEntityObj?.loginEntityObj?.Username,
                        entityObj?.personEntityObj?.loginEntityObj?.Password,
                        ref status,
                        ref message
                        );

                    storedProcOutPara(status, message);

                    return setQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<dynamic> GetDataAsync(string command, ICustomerEntity entityObj, Func<uspGetCustomerResultSet, ICustomerEntity> selector, Action<int?, string> storedProcOutPara = null)
        {
            try
            {
                return await Task.Run(() =>
                {
                    int? status = null;
                    string message = null;

                    var getQuery =
                    CreateDCInstance?.Value?.
                    uspGetCustomer(
                        command,
                        entityObj?.personEntityObj?.PersonId,
                        entityObj?.personEntityObj?.FirstName,
                        entityObj?.personEntityObj?.LastName,
                        entityObj?.personEntityObj?.communicationEntityObj?.MobileNo,
                        entityObj?.personEntityObj?.communicationEntityObj?.EmailId,
                        entityObj?.personEntityObj?.loginEntityObj?.Username,
                        entityObj?.personEntityObj?.loginEntityObj?.Password,
                        ref status,
                        ref message
                        )
                        .AsEnumerable()
                        .Select(selector)
                        .ToList();
                        ;

                        //storedProcOutPara(status, message);

                    return getQuery;
                });
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
