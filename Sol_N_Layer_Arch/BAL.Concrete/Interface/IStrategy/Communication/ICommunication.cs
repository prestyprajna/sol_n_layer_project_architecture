﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Concrete.Interface.IStrategy.Communication
{
    public interface ICommunication<TEntity> where TEntity : class
    {
        Task<Boolean> Send(TEntity entityObj);
    }
}
