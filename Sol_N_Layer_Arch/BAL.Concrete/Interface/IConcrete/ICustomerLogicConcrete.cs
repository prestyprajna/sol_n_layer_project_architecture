﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Concrete.Interface.IConcrete
{
    public interface ICustomerLogicConcrete:IDisposable
    {
        Task<dynamic> SendAsync(ICustomerEntity customerEntityObj);
    }
}
