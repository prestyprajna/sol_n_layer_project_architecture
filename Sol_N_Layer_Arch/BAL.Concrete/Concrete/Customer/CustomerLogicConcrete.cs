﻿using BAL.Concrete.Interface.IConcrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using BAL.Concrete.Interface.IStrategy.Communication;
using Entity.Model.Customer;

namespace BAL.Concrete.Concrete.Customer
{
    public class CustomerLogicConcrete : ICustomerLogicConcrete
    {
        private Lazy<ICommunication<ICustomerEntity>> _communicationEntityObj;

        #region Contructor
        public CustomerLogicConcrete(Lazy<ICommunication<ICustomerEntity>> communicationEntityObj)
        {
            this._communicationEntityObj = communicationEntityObj;
        }
        #endregion 

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        public async Task<dynamic> SendAsync(ICustomerEntity customerEntityObj)
        {
            try
            {
                return await _communicationEntityObj
                    ?.Value
                    ?.Send(customerEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
