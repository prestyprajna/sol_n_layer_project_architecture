﻿using BAL.Concrete.Interface.IStrategy.Communication;
using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Concrete.Strategy.Communication.Customer
{
    public class Sms : ICommunication<ICustomerEntity>
    {
        public async Task<bool> Send(ICustomerEntity entityObj)
        {
            return await Task.Run(() =>
            {
                return true;
            });
        }
    }
}
