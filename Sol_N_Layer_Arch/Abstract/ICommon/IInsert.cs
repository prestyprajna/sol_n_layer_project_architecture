﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.ICommon
{
    public interface IInsert<TEntity> where TEntity:class
    {
        Task<dynamic> InsertAsync(TEntity entityObj);
    }
}
