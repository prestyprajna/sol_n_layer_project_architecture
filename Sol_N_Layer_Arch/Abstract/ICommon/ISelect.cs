﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.ICommon
{
    public interface ISelect<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> SelectAsync(TEntity entityObj);
    }
}
