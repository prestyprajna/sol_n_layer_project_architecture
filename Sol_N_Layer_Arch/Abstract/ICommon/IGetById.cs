﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.ICommon
{
    public interface IGetById<TEntity> where TEntity : class
    {
        Task<TEntity> GetByIdAsync(TEntity entityObj);
    }
}
