﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.ICommon
{
    public interface IUpdate<TEntity> where TEntity : class
    {
        Task<dynamic> UpdateAsync(TEntity entityObj);
    }
}
