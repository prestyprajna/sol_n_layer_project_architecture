﻿using Abstract.Modules.Customer;
using DAL.Repository.IRepository.Customer;
using DAL.Repository.Repository.Customer;
using DAL.Repository.Repository_Factory.Customer;
using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal1.RepositoryFacade
{
    public class CustomerRepositoryFacade : CustomerAbstract
    {
        #region  declaration

        private Lazy<IAddCustomerRepository> _addCustomerRepositoryObj = null;
        private Lazy<DAL.Repository.IRepository.Customer.ILoginCustomerRepository> _loginCustomerRepositoryObj = null;

        #endregion

        #region  constructor

        public CustomerRepositoryFacade()
        {
            
        }

        #endregion

        #region  private property

        private Lazy<IAddCustomerRepository> AddCustomerRepositoryCreateInstance
        {
            get
            {
                return
                    _addCustomerRepositoryObj??
                    (_addCustomerRepositoryObj = new Lazy<IAddCustomerRepository>(() => CustomerRepositoryFactory.ExecuteRepository<AddCustomerRepository>(CustomerRepositoryFactory.RepositoyType.AddCustomerRepositoy)));

            }
        }

        private Lazy<DAL.Repository.IRepository.Customer.ILoginCustomerRepository> LoginCustomerRepositoryCreateInstance
        {
            get
            {
                return
                _loginCustomerRepositoryObj ??
                    (_loginCustomerRepositoryObj=new Lazy<ILoginCustomerRepository>(()=>CustomerRepositoryFactory.ExecuteRepository<LoginCustomerRepository>(CustomerRepositoryFactory.RepositoyType.LoginCustomerRepository)));
            }
        }
        #endregion 

        #region  public methods
       

        public async override Task<dynamic> CustomerRegistration(ICustomerEntity customerEntityObj)
        {
            try
            {
                return await this.AddCustomerRepositoryCreateInstance?.Value?.AddCustomerRegistration(customerEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override void Dispose()
        {
            _addCustomerRepositoryObj?.Value?.Dispose();
            _loginCustomerRepositoryObj?.Value?.Dispose();
        }

        public async override Task<dynamic> CustomerLogin(ICustomerEntity customerEntityObj)
        {
            try
            {
                return await this.LoginCustomerRepositoryCreateInstance?.Value?.CustomerLogin(customerEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

    }
}
