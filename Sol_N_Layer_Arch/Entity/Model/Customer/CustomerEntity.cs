﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Customer
{
    public class CustomerEntity: ICustomerEntity
    {
        public decimal? CustomerId { get; set; }

        public PersonEntity personEntityObj { get; set; }
    }
}
