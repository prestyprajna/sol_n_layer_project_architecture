﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class LoginEntity: ILoginEntity
    {
        public decimal? PersonId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
