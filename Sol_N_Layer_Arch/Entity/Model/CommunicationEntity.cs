﻿using Entity.IModel.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.Customer
{
    public class CommunicationEntity: ICommunicationEntity
    {
        public decimal? PersonId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
