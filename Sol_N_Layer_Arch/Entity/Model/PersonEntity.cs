﻿using Entity.IModel;
using Entity.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model
{
    public class PersonEntity: IPersonEntity
    {
        public decimal? PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public LoginEntity loginEntityObj { get; set; }

        public CommunicationEntity communicationEntityObj { get; set; }
    }
}
