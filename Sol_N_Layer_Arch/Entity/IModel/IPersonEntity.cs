﻿using Entity.Model;
using Entity.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel
{
    public interface IPersonEntity
    {
        decimal? PersonId { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

         LoginEntity loginEntityObj { get; set; }

         CommunicationEntity communicationEntityObj { get; set; }
    }
}
