﻿using Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Customer
{
    public interface ICustomerEntity
    {
         decimal? CustomerId { get; set; }

         PersonEntity personEntityObj { get; set; }
    }
}
