﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModel.Customer
{
    public interface ICommunicationEntity
    {
        decimal? PersonId { get; set; }

        string MobileNo { get; set; }

        string EmailId { get; set; }
    }
}
