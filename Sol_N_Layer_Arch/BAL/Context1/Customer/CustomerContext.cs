﻿using Dal1.RepositoryFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModel.Customer;
using BAL.Concrete.Interface.IConcrete;
using BAL.Concrete.Interface.IStrategy.Communication;
using BAL.Concrete.Concrete.Customer;
using BAL.Concrete.Strategy.Communication.Customer;

namespace BAL.Context1.Customer
{
    public class CustomerContext:CustomerRepositoryFacade
    {
        #region Declaration
        private Lazy<ICustomerLogicConcrete> _customerLogicConcreteObj = null;
        #endregion

        #region  constructor

        public CustomerContext():base()
        {

        }

        #endregion

        #region  public methods

        public async override Task<dynamic> CustomerRegistration(ICustomerEntity customerEntityObj)
        {
            try
            {
                dynamic resultSet=await  base.CustomerRegistration(customerEntityObj);

                if (resultSet is ICustomerEntity)
                {

                    // Mail Send
                    await this.SendCommunication((object)resultSet, () => new Mail());

                    // Send Sms
                    await this.SendCommunication((object)resultSet, () => new Sms());
                }

                return resultSet;
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public override Task<dynamic> CustomerLogin(ICustomerEntity customerEntityObj)
        {
            try
            {
                return base.CustomerLogin(customerEntityObj);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion

        #region  private methods

        private async Task SendCommunication(dynamic resultSet, Func<ICommunication<ICustomerEntity>> funcCommunication)
        {
            try
            {
                _customerLogicConcreteObj = new Lazy<ICustomerLogicConcrete>(() => new CustomerLogicConcrete(new Lazy<ICommunication<ICustomerEntity>>(() => funcCommunication())));

                // Send Mail
                await _customerLogicConcreteObj
                        ?.Value
                        ?.SendAsync(resultSet);
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
